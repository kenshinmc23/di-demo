package com.dh.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("en")
public class GreetingServiceIpml implements GreetingService {

    public static final String GREETHING = "Hello GreetingServiceIpml ";

    @Override
    public String sayGreeting() {
        return GREETHING;
    }
}
