package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class ConstructorGreetingServiceIpml implements GreetingService {

    public static final String GREETHING = "Constructor Hello GreetingServiceIpml ";

    @Override
    public String sayGreeting() {
        return GREETHING;
    }
}
