package com.dh.didemo.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Primary
@Profile("es")
public class SpanishGreetingServiceIpml implements GreetingService {

    public static final String GREETHING = "Hello SpanishGreetingServiceIpml ";

    @Override
    public String sayGreeting() {
        return GREETHING;
    }
}
