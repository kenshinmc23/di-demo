package com.dh.didemo.services;

import org.springframework.stereotype.Service;

@Service
public class GetterGreetingServiceIpml implements GreetingService {

    public static final String GREETHING = "Getter Hello GreetingServiceIpml ";

    @Override
    public String sayGreeting() {
        return GREETHING;
    }
}
