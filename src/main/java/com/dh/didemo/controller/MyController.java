package com.dh.didemo.controller;

import org.springframework.stereotype.Controller;

@Controller
public class MyController {

    public String hello() {

        String greeting = "Hello Spring";
        System.out.println(greeting);
        return greeting;
    }
}
