package com.dh.didemo.controller;

import com.dh.didemo.services.GreetingServiceIpml;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GetterBasedControllerTest {

    private GetterBasedController getterBasedController;

    @Before
    public void setUp() throws Exception {
        getterBasedController = new GetterBasedController();
        getterBasedController.setGreetingService(new GreetingServiceIpml());
    }

    @Test
    public void sayHello() {
        String greeting = getterBasedController.sayHello();
        Assert.assertEquals(GreetingServiceIpml.GREETHING, greeting);
    }
}